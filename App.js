import React, { useState, Component } from "react";
import { Text, View, TextInput, Image, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import { OpenGraphDisplay, OpenGraphParser } from 'react-native-opengraph-kit';

class CameraComponent extends Component {
  state = {
    fileData: 'https://www.searchpng.com/wp-content/uploads/2019/02/Men-Profile-Image-715x657.png'
  }

  onCameraPress = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };
        console.log('response', JSON.stringify(response));
        this.setState({
          fileData: 'data:image/jpeg;base64,' + response.data
        });
      }
    });
  }

  onImageResult = () => {
    this.setState({
      fileData: 'data:image/jpeg;base64,' + this.state.fileData
    })
  }

 render() {
    return (
      <View style={styles.container}>
        <View style={styles.box1}>
          <Image style={styles.profile} source={{uri: this.state.fileData}} />
        </View>
       <View style={styles.box2}>
       <Icon 
        raised
        name="camera-retro"
        type="font-awesome"
        onPress={this.onCameraPress}
        color="#8160e5">
        </Icon>
       </View>        
      </View>
    )
  }
}

class GraphComponent extends Component{
  state = {
    data: [{ 
    image: 'https://vooduvibe.com/wp-content/uploads/2018/05/OG-App-Logo1296x729-1.png',
    description: 'DISCOVER. EXPLORE. MINGLE » juuj is a new and exciting social networking experience that helps you make instant in-person connections with other people in your line-of-sight. juuj sparks spontaneous interactions between you and many people you encounter as you go about your daily life. Go on, juuj-up your social, dating, and professional life. Download it now!',
    url: 'https://vooduvibe.com/',
    type: 'website',
    title: 'juuj | Meet people you encounter, instantly' }],
  };

  handleTextChange = (event) => {
    OpenGraphParser.extractMeta(event.nativeEvent.text)
    .then((data) => {
        console.log(data);
        this.setState({ data });
    })
    .catch((error) => {
        console.log(error);
    });
  }

  render() {
    return (
    <View style={styles.input}>     
      <SafeAreaView>
        <View style={styles.container}>
            {this.state.data.map((meta, i) => <OpenGraphDisplay key={meta} data={meta} /> )}
            <TextInput
                placeholder='Enter something like: https://vooduvibe.com/'
                style={styles.textInput}
                onChange={this.handleTextChange}
            />
        </View>
      </SafeAreaView>
    </View>
    )
  }
}

const CameraGraphApp = () => {
  return (
    <ScrollView>
      <CameraComponent />       
      <GraphComponent />
    </ScrollView>
  );
}

export default CameraGraphApp;

const primaryColor = "#8160e5"
const secondaryColor = "#db2dcd"

const styles = StyleSheet.create({
  container:{
    marginTop: 24
  },
  box1:{
    alignSelf:"stretch",
    margin:32
  },
  box2:{
    position: 'absolute',
    top: 0,
    right: 48
  },
  profile:{
    width:'100%',
    aspectRatio: 1/1,
    height: undefined,
    borderWidth: 2,
    borderColor: secondaryColor,
  },
  input: {
    marginTop: 16,
    marginStart: 32,
    marginEnd: 32,
    alignSelf: "stretch"
  },
});